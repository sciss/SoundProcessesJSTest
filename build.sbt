lazy val deps = new {
  val main = new {
    val audioFile       = "2.4.2"
    val fscape          = "3.15.3"
    val laminar         = "0.13.1"  // must match LucreSwing!
    val lucre           = "4.6.4"
    val lucreSwing      = "2.10.1"
    // val plotly          = "0.8.1"
    val scalaJavaTime   = "2.1.0"   // must match SoundProcesses!
    val soundProcesses  = "4.14.9"
  }
}

lazy val root = project.in(file("."))
  .enablePlugins(ScalaJSPlugin)
  .settings(
    name := "SoundProcesses JS Test",
    scalaVersion := "2.13.11",
    // This is an application with a main method
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "com.raquo" %%% "laminar"               % deps.main.laminar,
      "de.sciss"  %%% "audiofile"             % deps.main.audioFile,
      "de.sciss"  %%% "fscape-lucre"          % deps.main.fscape,
      "de.sciss"  %%% "lucre-core"            % deps.main.lucre,
      "de.sciss"  %%% "lucre-expr"            % deps.main.lucre,
      "de.sciss"  %%% "lucre-swing"           % deps.main.lucreSwing,
      "de.sciss"  %%% "soundprocesses-core"   % deps.main.soundProcesses,
      "de.sciss"  %%% "soundprocesses-views"  % deps.main.soundProcesses,
      // "org.plotly-scala" %%% "plotly-render" % deps.main.plotly,
      "io.github.cquiroz" %%% "scala-java-time" % deps.main.scalaJavaTime,
    ),
    scalacOptions += "-deprecation",
    Compile / fastOptJS / artifactPath := baseDirectory.value / "lib" / "main.js",
    Compile / fullOptJS / artifactPath := baseDirectory.value / "lib" / "main.js",
  )

